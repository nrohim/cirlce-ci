const express = require('express')
const app = express()
const port = process.env.PORT || 3000 // update this line

app.use(express.json())

app.get('/', (request, response) =>{
  response.json({
    message:'Health check hello world success oke'
  })
})

app.listen(port, () =>{
  console.log(`server running at port ${port}`)
})